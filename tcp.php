<?php 
use Workerman\Worker;
require_once './core/Autoloader.php';

$tcp_worker = new Worker("tcp://0.0.0.0:8089");

$tcp_worker->onMessage=function($connection,$data)
{
	$connection->send($data);
};

Worker::runAll();
 ?>