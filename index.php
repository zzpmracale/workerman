<!DOCTYPE html>
<html>
<head>
  <title>登陆界面</title> 
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://apps.bdimg.com/libs/bootstrap/3.3.0/css/bootstrap.min.css">  
  <script src="http://apps.bdimg.com/libs/jquery/2.1.1/jquery.min.js"></script>
  <script src="http://apps.bdimg.com/libs/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <style>  
    body{  
    margin: 0 auto;
    text-align: center;
    }
  </style>
</head>
<body>



<input type="hidden" id="userid" value="<?php session_start();echo session_id(); ?>">
<div id="content" style="width:500px;height:200px;border:1px solid #ccc;margin:0 auto;text-align:left;overflow-y:auto;">

</div><br /><br />
<input type="text" placeholder="请输入内容" id="inputinfo"><br /><br />
<div style="display:none;">
<button type="submit" style="width:250px;" class="btn btn-default">发表</button>
</div>



<script type="text/javascript">

// 打开一个 web socket
var ws = new WebSocket("ws://192.168.18.59:8089");
ws.onopen = function(){
  var userid=$("#userid").val();
  // ws.send(userid);
};

ws.onmessage = function (evt){ 
  var received_msg = evt.data;
  $("#content").append(received_msg+"<br />");
  var div = document.getElementById('content');
  div.scrollTop = div.scrollHeight;
};

ws.onclose = function(){ 
  alert("onclose"); 
};


$(".btn").click(function() {
  var mydata=$("#inputinfo").val();
  $("#inputinfo").val("");
  $("#inputinfo").focus();
  wssend(mydata);
});
function wssend(data) {
  ws.send(data);
}

$(document).keydown(function(event){
  if (event.keyCode==13) {
    $(".btn").click();
  }
});
</script>
</body>
</html>
